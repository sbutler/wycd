require('dotenv').config()
const mailgun = require("mailgun-js");
const DOMAIN = process.env.MAILGUN_API_DOMAIN;
const mg = mailgun({apiKey: process.env.MAILGUN_API_KEY, domain: process.env.MAILGUN_API_DOMAIN});
const data = {
	from: 'Excited User <me@samples.mailgun.org>',
	to: 'sambutler12@gmail.com',
	subject: 'Hello',
	text: 'Testing some Mailgun awesomness!'
};
mg.messages().send(data, function (error, body) {
	console.log(body);
});


const MAILGUN_API_KEY = process.env.MAILGUN_API_KEY
var MAILGUN_API_DOMAIN = process.env.MAILGUN_API_DOMAIN
// var EMAIL_RECIPIENT = process.env.EMAIL_RECIPIENT

// var sendToSlack = (name, email, phone, message) => {
// let fields = [
//   {
//     title: "Name", value: name, short: false
//   },
//   {
//     title: "Email", value: email, short: false
//   },
//   {
//     title: "Phone", value: phone, short: false
//   }
// ]
// if (message) fields.push({ title: "Message", value: message, short: false })
// let data = {
//   icon_emoji: ":nimbus:",
//   attachments: [
//     {
//       fallback: "New web enquiry",
//       color: "#36a64f",
//       author_name: "Web enquiry",
//       fields: fields
//     }
//   ]
// }
//
// request({
//   url: SLACK_URL,
//   method: "POST",
//   json: true,
//   body: data
// }, function (error, response, body) {
//   if (error) console.log('error', error)
// })
// }
//
// var sendToTrello = (name, email, phone, message) => {
// var t = new Trello(TRELLO_KEY, TRELLO_TOKEN)
// let desc = `
// Name: ${name}
// Email: ${email}
// Phone: ${phone}
// Message: ${message}
// `
// t.post('/1/cards/', {
//   idList: TRELLO_LIST_ID,
//   name: '***NEW*** ' + name,
//   desc: desc
// }, (err) => {
//   if (err) console.log(err)
//
// })
// }

var sendToEmail = (name, email, phone, message) => {
var mailgun = require('mailgun-js')({apiKey: MAILGUN_API_KEY, domain: MAILGUN_API_DOMAIN})
let data = {
  from: `${name} <${email}>`,
  to: `<${email}>`,
  subject: 'New web request from ' + name,
  text: `
    Name: ${name}
    Email: ${email}
    Phone: ${phone}
    Message: ${message}
  `
}
mailgun.messages().send(data, function (error, body) {
  if (error) console.log(error)
})
}

export function handler (event, context, callback){
console.log('called', event)
var origin = event["headers"]["origin"]
var amp_source = event["queryStringParameters"]["__amp_source_origin"]
var parser = new Multipart(event)

parser.on('field',function(key, value){
  console.log('received field', key, value);
})
// parser.on('file',function(file){
//   //file.headers['content-type']
//   file.pipe(fs.createWriteStream(__dirname+"/downloads/"+file.filename));
// })

parser.on('finish',function(result){
  let fields = result.fields
  console.log('result.fields', result.fields)
  try {
    sendToSlack(fields.Name, fields.Email, fields.Phone, fields.Message)
    sendToTrello(fields.Name, fields.Email, fields.Phone, fields.Message)
    sendToEmail(fields.Name, fields.Email, fields.Phone, fields.Message)
  } catch (e) {
    console.log('e', e)
  }

  let responseBody = {result: "Success."} // or {errorMessage: error_description}
  let response = {
    "isBase64Encoded": false,
    "headers": {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
      'Access-Control-Allow-Methods': 'POST',
      'Access-Control-Allow-Origin': origin,
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Expose-Headers': 'AMP-Access-Control-Allow-Source-Origin',
      'AMP-Access-Control-Allow-Source-Origin': amp_source
    },
    "statusCode": 200,
    "body": JSON.stringify(responseBody)
  }
  callback(null, response)
});
}
