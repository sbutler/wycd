require('dotenv').config();
var querystring = require('querystring');
const SparkPost = require('sparkpost');
const client = new SparkPost(process.env.SPARKPOST);

exports.handler = function(event, context, callback) {
  console.log(event);
  const data = querystring.parse(event.body);
  console.log(data);
  var role = data.role;

  client.transmissions.send({
    options: {
      sandbox: true
    },
    content: {
      from: 'admin@mail.whatican.org',
      reply_to: "Sam <admin@whatican.org>",
      subject: 'Hello, World!',
      html:'<html><body><p>Testing SparkPost - the world\'s most awesomest email service!</p></body></html>'
      },
    recipients: [
      {
        address: data.email
      },
      {
        address: "Sam <admin@whatican.org>"
      },
    ]
  })
  .then(data => {
    console.log('Woohoo! You just sent your first mailing!');
    console.log(data);
  })
  .catch(err => {
    console.log('Whoops! Something went wrong');
    console.log(err);
  });


}
