// const Mailgun = require('mailgun-js');
// const Airtable = require('airtable');
// require('querysting');
var querystring = require('querystring');
// console.log(JSON.stringify(event));
require('dotenv').config();
var MAILGUN_API_KEY = process.env.MAILGUN_API_KEY;
var MAILGUN_API_DOMAIN = process.env.MAILGUN_API_DOMAIN;
var mailgun = require('mailgun-js')({apiKey: MAILGUN_API_KEY, domain: MAILGUN_API_DOMAIN});

exports.handler = async (event, context, callback) => {
  // Only allow POST
  if (event.httpMethod !== "POST") {
    return { statusCode: 405, body: "Method Not Allowed" };
  }

  // When the method is POST, the name will no longer be in the event’s
  // queryStringParameters – it’ll be in the event body encoded as a query string
  const data = querystring.parse(event.body);
  console.log(data);
  const name = data.name || "World";

  if(data.antibot.length>0){
         return callback(new Error('Forbidden access'))
     }

     let messageData = {
         from: data.email,
         to: data.email,
         subject: `Message received from ${data.name}`,
         text: `${data.message}`
     }

     mailgun.messages().send(messageData, function (error) {
         if (error) {
     		return callback(error);
     	} else {
     		return callback(null, {
 			    statusCode: 200,
 			    body: 'success'
 	    	});
     	}
     })
}
